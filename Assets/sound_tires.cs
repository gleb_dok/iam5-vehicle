﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NWH.VehiclePhysics2;

public class sound_tires : MonoBehaviour
{
    public VehicleController vc;

    public AK.Wwise.Event tireevent;

    // Start is called before the first frame update
    void Start()
    {

        tireevent.Post(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        AkSoundEngine.SetRTPCValue("RTPC_speed", vc.Speed, gameObject);
    }
}
