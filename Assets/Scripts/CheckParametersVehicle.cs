﻿using System;
using System.Collections;
using System.Collections.Generic;
using NWH.VehiclePhysics2;
using UnityEngine;

public class CheckParametersVehicle : MonoBehaviour
{
    public VehicleController vc;

    public AK.Wwise.Event enginesound;
    // Start is called before the first frame update
    void Start()
    {
        vc = gameObject.GetComponent<VehicleController>();
        enginesound.Post(gameObject);
    }

    // Update is called once per frame

    void OnGUI()
    {
       
        
        GUI.Label (new Rect (50, 25, 150, 30), "RPM: " + vc.powertrain.engine.RPM.ToString());
        GUI.Label (new Rect (50, 45, 150, 30), "Load: " + vc.powertrain.engine.GetLoad().ToString());
        GUI.Label (new Rect (50, 65, 150, 30), "Power: " + vc.powertrain.engine.generatedPower.ToString());
        GUI.Label (new Rect (50, 85, 150, 30), "Gear: " + vc.powertrain.transmission.Gear.ToString());
        GUI.Label (new Rect (50, 105, 150, 30), "Speed: " + vc.Speed.ToString());
        
       
      
    }

    private void Update()
    {
        AkSoundEngine.SetRTPCValue("RTPC_rpm", vc.powertrain.engine.RPM, gameObject);
        AkSoundEngine.SetRTPCValue("RTPC_power", vc.powertrain.engine.generatedPower, gameObject);
        AkSoundEngine.SetRTPCValue("RTPC_load", vc.powertrain.engine.GetLoad(), gameObject);
        AkSoundEngine.SetRTPCValue("RTPC_gear", vc.powertrain.transmission.Gear, gameObject);
        
       
    }
}
